package round

import "math"

func Getsee(f float64, tarr ...int) float64 {
	l := 5. //加进1用
	c := 1. //位移用
	t := 0  //选填参数已经是数组，所以要处理一下
	for _, n := range tarr {
		t = n
	}
	if t > 0 {
		//返回小数点右边
		for n := 0; n <= t; n++ {
			l = l / 10
		}
		for n := 0; n < t; n++ {
			c = c * 10
		}
		f = math.Floor((f+l)*c) / c
	} else if t < 0 {
		//返回小数点左边
		for n := t; n < -1; n++ {
			l = l * 10
		}
		for n := t; n < 0; n++ {
			c = c * 10
		}
		//这里本来是 (f + l) / c * c 就可以了，但是因为是浮点数，不会变成0，所以要转成整数型
		ff := int(f)
		ll := int(l)
		cc := int(c)
		fff := (ff + ll) / cc * cc
		f = float64(fff)
	} else {
		//不保留小数
		f = math.Floor(f + 0.5)
	}
	return f
}
