# go-package

#### 介绍
放一些自己做的轮子包

#### 安装  
如果还没有go.mod文件，那自己建一个并在首行写上这句module gitee.com/startbbq/go-package.git  
然后执行以下go命令  
go get -v gitee.com/startbbq/go-package.git  

#### 四舍五入  
 ``` 
import (  
	"fmt"  
  
	"gitee.com/startbbq/go-package/round"  
)  
  
f := 56789.556789  
t := -2  
fmt.Println(round.Getsee(f, t))//留百位  
fmt.Println(round.Getsee(56749.556789))//留个位  
```
#### 转为字符串  
```
import "gitee.com/startbbq/go-package/strval"  
str := strval.String(interface)
```
#### hash哈希加密码验证  
```
import "gitee.com/startbbq/go-package/hash"  
hash := hash.HashAndSalt([]byte("123")) //加密
fmt.Println("hash过的密码", hash)
pwdMatch := hash.ComparePasswords(hash, []byte("123")) //验证
if pwdMatch {
	fmt.Println("验证密码成功")
}
```
#### SGX包cgo，linux系统  
```
首先，我们需要在sgxnb.go文件中导入Intel SGX SDK的包。我们可以使用以下命令来安装Intel SGX SDK：  

sudo apt-get install libsgx-enclave-common libsgx-enclave-common-dev libsgx-dcap-ql libsgx-dcap-ql-dev  

//..
import "gitee.com/startbbq/go-package/sgxnb"  //引包
sgxv := sgxnb.EncryptSGX([]byte, error) //加密
fmt.Println("sgx过的加密", sgxv)

```